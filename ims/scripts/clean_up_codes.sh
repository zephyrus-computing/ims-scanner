#!/bin/bash
# Ensure that the user executing this script is 'imsuser'
if [ "$USER" != "imsuser" ]; then
    echo "This script needs to be run as imsuser!";
    exit;
fi
folder=$(dirname $0); # Should be /etc/zc/ims/scripts
parentfolder=$(dirname $folder); # Should be /etc/zc/ims
logfile=$(grep -e "INSTALL_LOG_PATH" "$parentfolder/ims/settings.py" | cut -d'=' -f2);
if [[ ! -d $(dirname $logfile) ]]; then
    logfile="$parentfolder/install.log";
fi
# Get the Media QR Codes folder
targetfolder=/etc/zc/ims/media/qrcodes;
if [[ ! -d $targetfolder ]]; then
    [[ -d "$partfolder/media/qrcodes" ]] && targetfolder="$parentfolder/media/qrcodes" || targetfolder='';
    if [[ $targetfolder == '' ]]; then
        echo "ERROR: Media QRCodes folder not found!" | tee -a $logfile;
        exit 1;
    fi
fi
# Execute find with delete flag; delete files older than 10 minutes
find "$targetfolder" -type f -mmin +10 -delete;
