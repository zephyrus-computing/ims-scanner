#! /bin/bash
folder=$(dirname $(readlink -f $0)) # Should be /etc/zc/ims/scripts
parentfolder=$(dirname $folder) # Should be /etc/zc/ims
# Find the scanner.conf file
if [[ -f '/etc/zc/ims/config/scanner.conf' ]]; then
    configfile='/etc/zc/ims/config/scanner.conf'
else
    [[ -f "$parentfolder/ims/config/scanner.conf" ]] && configfile="$parentfolder/ims/config/scanner.conf" || configfile=''
    if [[ $configfile == '' ]]; then
        echo "ERROR: IMS scanner.conf File Not Found!"
        exit 1
    fi
fi
logfile=$(grep -e "INSTALL_LOG_PATH" "$parentfolder/ims/settings.py" | cut -d'=' -f2)
if [[ ! -d $(dirname $logfile) ]]; then
    [[ -d "$parentfolder/logs/" ]] && logfile="$parentfolder/logs/install.log" || logfile="$folder/install.log"
fi
if [ $1 ]; then
    if [[ ${1,,} == "y" ]]; then
        enable='y'
    else
        enable='n'
    fi
    if [ $7 ]; then
        errorcorrection=$2
        boxsize=$3
        border=$4
        foreground=$5
        background=$6
        certpath=$7
    fi
else
    read -p "Enable Scanner Addon (Y/n): " enable
    if [[ $enable == "" ]]; then enable='Y'; fi
fi
if [[ ${enable,,} == "y" ]]; then
    if [ ! $7 ]; then
        enable="True"
        echo "Error Correction (L = >7%, M = >15%, Q = >25%, H = >30%)"
        read -p "(l/M/q/h):" errorcorrection
        if [[ ! $errorcorrection =~ [l,m,q,h,L,M,Q,H] ]]; then errorcorrection="M"; fi
        echo "Box Size (how many pixels each 'box' of the QR code is)"
        read -p "(1-10 default 4):" boxsize
        if [[ ! $boxsize =~ ^(1[0-9]|[1-9])$ ]]; then boxsize=4; fi
        echo "Border (how many boxes thick the border should be (4 is minimum))"
        read -p "(4-10 default 4):" border
        if [[ ! $border =~ ^(1[0-9]|[4-9])$ ]]; then border=4; fi
        echo "Foreground RGB color tuple ('(0,0,0)' black, '(255,255,255)' white, include '()')"
        read -p "'(0,0,0)':" foreground
        if [[ ! $foreground =~ ^\((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?),(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?),(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\)$ ]]; then foreground="(0,0,0)"; fi
        echo "Background RGB color tuple ('(0,0,0)' black, '(255,255,255)' white, include '()')"
        read -p "'(255,255,255)':" background
        if [[ ! $background =~ ^\((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?),(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?),(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\)$ ]]; then background="(255,255,255)"; fi
        echo "Local path to issuing CA certificate for the web server SSL cert"
        read -p "(/etc/ssl/certs/rootCA.crt):" certpath
        if [[ $certpath == "" ]]; then certpath="/etc/ssl/certs/rootCA.crt"; fi
    fi
else
    enable="False"
    errorcorrection="M"
    boxsize=4
    border=4
    foreground="(0,0,0)"
    background="(0,0,0)"
    certpath="/etc/ssl/certs/rootCA.crt"
fi
echo "$(date)" >> $logfile
echo "Configuring Scanner..." >> $logfile
# Update scanner.conf with Scanner settings
echo "Configuring ims settings ($configfile)" >> $logfile
cat $configfile >> $logfile
cp -f $configfile $configfile.old
echo -e "# QR Code Generation Settings\nADDON_SCANNER_ERROR_CORRECTION = \"$errorcorrection\" # L = >7%, M = >15%, Q = >25%, H = >30%\nADDON_SCANNER_BOX_SIZE = $boxsize # how many pixels each “box” of the QR code is\nADDON_SCANNER_BORDER = $border # how many boxes thick the border should be (4 is minimum)\nADDON_SCANNER_FOREGROUND = $foreground # RGB color tuple\nADDON_SCANNER_BACKGROUND = $background # RGB color tuple\n\n###\n\n# Additional Settings\nADDON_SCANNER_KEYWORD = 'ims'\nADDON_SCANNER_ENABLED = $enable\n\n###\n\n" > $configfile
echo "Configuring ims Environment..." >> $logfile
# Run as imsuser, install packages and migrate
su -c "source $parentfolder/venv/bin/activate; pip install -r $parentfolder/ims/addons/scanner/requirements.txt; python $parentfolder/ims/config/generate.py; python $parentfolder/manage.py migrate; python $parentfolder/manage.py shell < $parentfolder/scripts/create_scanner_builtin_groups.py; if [ -f \"$certpath\" ]; then python $parentfolder/manage.py shell -c \"import requests; trust=requests.certs.where(); file=open('$certpath', 'r'); cert=file.read(); file.close(); file=open(trust, 'a'); file.writelines(cert); file.close();\"; fi; deactivate;" imsuser | tee -a $logfile
