#!/usr/bin/env python

from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType

scanReadonly = Group.objects.get_or_create(name="Scanner ReadOnly")[0]
scanManager = Group.objects.get_or_create(name="Scanner Manager")[0]
scanAdmin = Group.objects.get_or_create(name="Scanner Admin")[0]

ct = ContentType.objects.get_or_create(app_label="scanner",model="qrcode")[0]

p = Permission.objects.get_or_create(name='Can view qrcode',codename='view_qrcode',content_type=ct)[0]
scanReadonly.permissions.add(p)
scanManager.permissions.add(p)
scanAdmin.permissions.add(p)

p = Permission.objects.get_or_create(name='Can change qrcode',codename='change_qrcode',content_type=ct)[0]
scanManager.permissions.add(p)
scanAdmin.permissions.add(p)

p = Permission.objects.get_or_create(name='Can add qrcode',codename='add_qrcode',content_type=ct)[0]
scanAdmin.permissions.add(p)

scanReadonly.save()
scanManager.save()
scanAdmin.save()
