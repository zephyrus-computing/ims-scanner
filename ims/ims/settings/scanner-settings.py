import environ
import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
envroot = environ.Path(BASE_DIR)
env = environ.Env()
environ.Env.read_env(os.path.join(BASE_DIR, '.env'))

ADDON_SCANNER_ERROR_CORRECTION = env.str('ADDON_SCANNER_ERROR_CORRECTION', default='M')
ADDON_SCANNER_BOX_SIZE = env.int('ADDON_SCANNER_BOX_SIZE', default=5)
ADDON_SCANNER_BORDER = env.int('ADDON_SCANNER_BORDER', default=4)
ADDON_SCANNER_FOREGROUND = env.list('ADDON_SCANNER_FOREGROUND', default=(0,0,0))
ADDON_SCANNER_BACKGROUND = env.list('ADDON_SCANNER_BACKGROUND', default=(255,255,255))
ADDON_SCANNER_KEYWORD = 'ims'

# Possibly to be deprecated; will fix otherwise
PWA_SERVICE_WORKER_PATH = os.path.join(BASE_DIR, 'static/scanner/js', 'serviceworker.js')
PWA_APP_DESCRIPTION = 'Inventory Management System'
PWA_APP_THEME_COLOR = '#000000'
PWA_APP_BACKGROUND_COLOR = '#ffffff'
PWA_APP_DISPLAY = 'standalone'
PWA_APP_SCOPE = '/'
PWA_APP_ORIENTATION = 'any'
PWA_APP_START_URL = '/'
PWA_APP_STATUS_BAR_COLOR = 'default'
PWA_APP_ICONS = [
    {
        'src': 'media/IMS Logo1.png',
        'sizes': '90x27'
    }
]
PWA_APP_ICONS_APPLE = [
    {
        'src': 'media/IMS Logo1.png',
        'sizes': '90x27'
    }
]
PWA_APP_SPLASH_SCREEN = [
    {
        'src': 'media/IMS Logo1.png',
        'media': '(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)'
    }
]
PWA_APP_DIR = 'ltr'
PWA_APP_LANG = 'en-US'