from django.conf import settings
from django.test import TestCase
from hashlib import sha256
from ims.addons.scanner.classes import QRCodeContent, _token

class QrscanClassesUnitTests(TestCase):
    def setUp(self):
        self.url = 'https://localhost/inventory/part/banana/'
        self.action = 'go'

    def test_token_creation(self):
        result = _token(self.url, self.action)
        self.assertEqual('12bceb79ce0bb8a17e4fa5013ac9815c8bcaad6dd145ab07554a3152bd69b0bc', result)
        result = _token(self.action, self.url)
        self.assertNotEqual('12bceb79ce0bb8a17e4fa5013ac9815c8bcaad6dd145ab07554a3152bd69b0bc', result)

    def test_qrcode_content(self):
        content = QRCodeContent(self.url, self.action)
        self.assertEqual(self.url, content.url)
        self.assertEqual(self.action, content.action)
        self.assertEqual('12bceb79ce0bb8a17e4fa5013ac9815c8bcaad6dd145ab07554a3152bd69b0bc', content.token)
        self.assertEqual('1', content.version)
