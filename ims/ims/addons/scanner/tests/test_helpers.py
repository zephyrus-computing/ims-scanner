import django
django.setup()
from django.conf import settings
from django.test import TestCase, override_settings
from hashlib import sha256
from qrcode.constants import ERROR_CORRECT_L, ERROR_CORRECT_M, ERROR_CORRECT_Q, ERROR_CORRECT_H
from random import randint
from inventory.models import Part, Storage
import ims.addons.scanner.helpers as helpers

class LookupMethodTests(TestCase):
    def setUp(self):
        for i in range(10,15):
            Part.objects.create(name='Test-View-Part-{}'.format(i), description='This is a special view TestCase test part', sku=str(i*10).zfill(5), price=10.00, cost=5.00)
            Storage.objects.create(name='Test-View-Storage-{}'.format(i), description='This is a special view TestCase test storage')
        Part.objects.create(name='Test_View_Part_X', description='This is a view TestCase test part', sku=str(randint(0,100000)).zfill(5), price=10.00, cost=5.00)
        Storage.objects.create(name='Test_View_Storage_X', description='This is a view TestCase test storage')

    def test_find_part(self):
        result = helpers.find_part('Test_View_Storage_X-Test_View_Part_X')
        self.assertEqual(result, Part.objects.get(name='Test_View_Part_X'))
        i = randint(10,14)
        result = helpers.find_part('Test-View-Storage-{}-Test-View-Part-{}'.format(i,i))
        self.assertEqual(result, Part.objects.get(name='Test-View-Part-{}'.format(i)))
        result = helpers.find_part('Test-View-Storage-Y-Test-View-Part-Y')
        self.assertEqual(result, '')

    def test_find_storage(self):
        result = helpers.find_storage('Test_View_Storage_X-Test_View_Part_X')
        self.assertEqual(result, Storage.objects.get(name='Test_View_Storage_X'))
        i = randint(10,14)
        result = helpers.find_storage('Test-View-Storage-{}-Test-View-Part-{}'.format(i,i))
        self.assertEqual(result, Storage.objects.get(name='Test-View-Storage-{}'.format(i)))
        result = helpers.find_part('Test-View-Storage-Y-Test-View-Part-Y')
        self.assertEqual(result, '')

class QrCodeHelperMethodTests(TestCase):
    def test_generate(self):
        name = 'Test_View_Part_0'
        url = 'http://testserver/inventory/part/Test_View_Part_0/'
        action = 'go'
        expected = settings.MEDIA_URL + "qrcodes/" + name
        result = helpers.generate(name, url, action)
        if expected not in result:
            self.fail()
        name = 'Test_View_Storage_1-Test_View_Part_1'
        url = 'http://testserver/api/addpart/1/1/5/'
        action = 'do'
        expected = settings.MEDIA_URL + "qrcodes/" + name
        result = helpers.generate(name, url, action)
        if expected not in result:
            self.fail()
    
    def test_error_correction(self):
        result = helpers.error_correction()
        self.assertTrue(result == ERROR_CORRECT_M)
        with override_settings(ADDON_SCANNER_ERROR_CORRECTION = "L"):
            result = helpers.error_correction()
            self.assertTrue(result == ERROR_CORRECT_L)
        with override_settings(ADDON_SCANNER_ERROR_CORRECTION = "Q"):
            result = helpers.error_correction()
            self.assertTrue(result == ERROR_CORRECT_Q)
        with override_settings(ADDON_SCANNER_ERROR_CORRECTION = "H"):
            result = helpers.error_correction()
            self.assertTrue(result == ERROR_CORRECT_H)
        with override_settings(ADDON_SCANNER_ERROR_CORRECTION = "Z"):
            result = helpers.error_correction()
            self.assertTrue(result == ERROR_CORRECT_M)

    def test_decode(self):
        qrcode = 'eyJ2ZXJzaW9uIjoiMSIsInRva2VuIjoiN2JlMTgzYWI4Y2YxYWExYzkxMTcwNDIzNzBiYzQzNDQxNjZjZTgxYWU4NGFlYTkwYWU3ZWFiY2Q5OTJhMGFhMCIsImFjdGlvbiI6ImRvIiwidXJsIjoiaHR0cDovL3Rlc3RzZXJ2ZXIvYXBpL2FkZHBhcnQvMS8xLzUvIn0='
        result = helpers.decode(qrcode)
        self.assertTrue(result['action'] == 'do')
        self.assertTrue(result['url'] == 'http://testserver/api/addpart/1/1/5/')
        qrcode = 'eyJ2ZXJzaW9uIjoiMSIsInRva2VuIjoiZTVhNjNmYmViZWE2ZmRmNjFjNmE0NDQ1NDFmN2Q5NzNlNzI3NTA0YjFkMWM2MmMwMjcwNGViZGMzNDJkMTJkZCIsImFjdGlvbiI6ImdvIiwidXJsIjoiaHR0cDovL3Rlc3RzZXJ2ZXIvaW52ZW50b3J5L3BhcnQvVGVzdF9WaWV3X1BhcnRfMC8ifQ=='
        result = helpers.decode(qrcode)
        self.assertTrue(result['action'] == 'go')
        self.assertTrue(result['url'] == 'http://testserver/inventory/part/Test_View_Part_0/')
        result = helpers.decode('banana')
        self.assertTrue(result == '')

    def test_compare_token(self):
        salt = settings.SECRET_KEY
        action = "go"
        url = "http://testserver/inventory/part/Test-View-Part-0/"
        token = sha256(salt.encode('utf-8') + action.encode('utf-8') + url.encode('utf-8')).hexdigest()
        data = {
            "action": action,               
            "url": url,
            "token": token
        }
        result = helpers.compare_token(data)
        self.assertTrue(result)
        data = {
            "action": "do",
            "url": url,
            "token": token
        }
        result = helpers.compare_token(data)
        self.assertFalse(result)

