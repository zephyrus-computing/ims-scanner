import django
django.setup()
from django.contrib.auth.models import User, Permission
from django.contrib.contenttypes.models import ContentType
from django.test import TestCase, override_settings
from random import randint
from requests.exceptions import ConnectionError
from rest_framework import status
from inventory.models import Part, Storage, PartStorage, Assembly, Kit, KitPartStorage

module = 'scanner'
baseurl = '/{}/'.format(module)

class QrscanGenerateViewTests(TestCase):
    def setUp(self):
        ct = ContentType.objects.get_or_create(app_label='scanner', model='qrcode')[0]
        Permission.objects.get_or_create(content_type=ct, name='Can view qrcode', codename='view_qrcode')
        Permission.objects.get_or_create(content_type=ct, name='Can change qrcode', codename='change_qrcode')
        Permission.objects.get_or_create(content_type=ct, name='Can add qrcode', codename='add_qrcode')
        for i in range(0,5):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a view TestCase test part', sku=str(i*10).zfill(5), price=10.00, cost=5.00)
            Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is a view TestCase test storage')
            PartStorage.objects.create(storage=Storage.objects.get(name='Test_View_Storage_{}'.format(i)), part=Part.objects.get(name='Test_View_Part_{}'.format(i)), count=1)
            Kit.objects.create(name='Test_View_Kit_{}'.format(i), description='This is a view TestCase test kit', sku=str(i*11).zfill(5), price=20.0)
            KitPartStorage.objects.create(kit=Kit.objects.get(name='Test_View_Kit_{}'.format(i)), partstorage=PartStorage.objects.last(), count=1)
        for i in range(0,2):
            j = randint(0,4)
            p = Part.objects.get(name='Test_View_Part_{}'.format(j))
            k = randint(1,3)
            for l in range(0,k):
                m = randint(0,4)
                Assembly.objects.create(parent=p, part=Part.objects.get(name='Test_View_Part_{}'.format(m)), count=1)

    def test_qrscan_home(self):
        response = self.client.get(baseurl)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertRedirects(response, '/accounts/login/?next={}'.format(baseurl))
        self.client.force_login(User.objects.get_or_create(username='teststaff',is_staff=1)[0])
        response = self.client.get(baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, '{}/home.html'.format(module))

    def test_qrscan_create(self):
        response = self.client.get('{}create/'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        response = self.client.get('{}ajax_create/'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        user = User.objects.get_or_create(username='test_code_creator')[0]
        user.user_permissions.add(Permission.objects.get(codename='add_qrcode'))
        self.client.force_login(user)
        response = self.client.get('{}create/'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, '{}/create.html'.format(module))
        ass = Assembly.objects.first()
        response = self.client.get('{}ajax_create/?m=assembly&t={}'.format(baseurl, ass.parent.name))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, '{}/code.html'.format(module))
        response = self.client.get('{}ajax_create/?m=part&t=Test_View_Part_0&c=false'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, '{}/code.html'.format(module))
        response = self.client.get('{}ajax_create/?m=storage&t={}'.format(baseurl, Storage.objects.first().name))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, '{}/code.html'.format(module))
        response = self.client.get('{}ajax_create/?m=banana&t=yellow&d=get&n=0'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        response = self.client.get('{}ajax_create/?m=partstorage&t={}&d=get&n=0'.format(baseurl, PartStorage.objects.last()))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        response = self.client.get('{}ajax_create/?m=partstorage&t={}&d=add&n=5&c=true'.format(baseurl, PartStorage.objects.first()))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, '{}/code.html'.format(module))
        response = self.client.get('{}ajax_create/?m=partstorage&t={}&d=sub&n=3'.format(baseurl, PartStorage.objects.last()))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, '{}/code.html'.format(module))
        response = self.client.get('{}ajax_create/?m=kit&t={}&d=get'.format(baseurl, Kit.objects.first().name))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, '{}/code.html'.format(module))
        response = self.client.get('{}ajax_create/?m=kit&t={}&d=sub&n=2&c=true'.format(baseurl, Kit.objects.first().name))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, '{}/code.html'.format(module))
        # response = self.client.get('{}create/?m=kit&t={}&d=add&n=1'.format(baseurl, Kit.objects.first().name))
        # self.assertEqual(response.status_code, status.HTTP_200_OK)
        # self.assertTemplateUsed(response, '{}/code.html'.format(module))

    def test_qrscan_ajax(self):
        response = self.client.get('{}ajax/'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        user = User.objects.get_or_create(username='test_code_creator')[0]
        user.user_permissions.add(Permission.objects.get(codename='add_qrcode'))
        self.client.force_login(user)
        response = self.client.get('{}ajax/'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, '{}/dropdown_list_options.html'.format(module))
        response = self.client.get('{}ajax/?selected=assembly'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Test_View_Part_')
        response = self.client.get('{}ajax/?selected=part'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Test_View_Part_')
        response = self.client.get('{}ajax/?selected=storage'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Test_View_Storage_')
        response = self.client.get('{}ajax/?selected=partstorage'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Test_View_Part_')
        self.assertContains(response, 'Test_View_Storage_')
        response = self.client.get('{}ajax/?selected=kit'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Test_View_Kit_')


class QrscanScanViewTests(TestCase):
    def setUp(self):
        ct = ContentType.objects.get_or_create(app_label='scanner', model='qrcode')[0]
        Permission.objects.get_or_create(content_type=ct, name='Can view qrcode', codename='view_qrcode')
        Permission.objects.get_or_create(content_type=ct, name='Can change qrcode', codename='change_qrcode')
        Permission.objects.get_or_create(content_type=ct, name='Can add qrcode', codename='add_qrcode')
        for i in range(5,10):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a view TestCase test part', sku=str(i*10).zfill(5), price=10.00, cost=5.00)
            Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is a view TestCase test storage')
            PartStorage.objects.create(storage=Storage.objects.get(name='Test_View_Storage_{}'.format(i)), part=Part.objects.get(name='Test_View_Part_{}'.format(i)), count=1)
        pass

    def test_qrscan_scan(self):
        response = self.client.get('{}scan/'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertRedirects(response, '/accounts/login/?next={}scan/'.format(baseurl))
        self.client.force_login(User.objects.get_or_create(username='teststaff',is_staff=1)[0])
        response = self.client.get('{}scan/'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, '{}/scan.html'.format(module))

    def test_qrscan_scanned(self):
        response = self.client.get('{}scanned/'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        response = self.client.get('{}scanned/qrcode=banana'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        response = self.client.get('{}scanned/qrcode=eyJ2ZXJzaW9uIjoiMSIsInRva2VuIjoiN2JlMTgzYWI4Y2YxYWExYzkxMTcwNDIzNzBiYzQzNDQxNjZjZTgxYWU4NGFlYTkwYWU3ZWFiY2Q5OTJhMGFhMCIsImFjdGlvbiI6ImRvIiwidXJsIjoiaHR0cDovL3Rlc3RzZXJ2ZXIvYXBpL2FkZHBhcnQvMS8xLzUvIn0='.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertNotEqual(response.headers['Location'], '/scanner/scan/')
        user = User.objects.get_or_create(username='test_user')[0]
        self.client.force_login(user)
        response = self.client.get('{}scanned/qrcode='.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.headers['Location'], '/scanner/scan/')
        response = self.client.get('{}scanned/qrcode=eyJ2ZXJzaW9uIjoiMSIsInRva2VuIjoiN2JlMTgzYWI4Y2YxYWExYzkxMTcwNDIzNzBiYzQzNDQxNjZjZTgxYWU4NGFlYTkwYWU3ZWFiY2Q5OTJhMGFhMCIsImFjdGlvbiI6ImRvIiwidXJsIjoiaHR0cDovL3Rlc3RzZXJ2ZXIvYXBpL2FkZHBhcnQvMS8xLzUvIn0='.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.headers['Location'], '/scanner/scan/')
        response = self.client.get('{}scanned/qrcode=eyJ2ZXJzaW9uIjoiMSIsInRva2VuIjoiMzA3NTc1NjJiMjM3MWI3N2YxMDIyOWNjNmU2ZTkwNTY0MDVmODQ3ZmM3ZTQwYWM2MWU4OWM0ZmIyZmQ4M2U5ZiIsImFjdGlvbiI6ImRvIiwidXJsIjoiaHR0cDovL3Rlc3RzZXJ2ZXIvYXBpL3YxL2ludmVudG9yeS9wYXJ0c3RvcmFnZS8xNzQvYWRkLzEvIn0='.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.headers['Location'], '/scanner/scan/')
        user = User.objects.get_or_create(username='test_code_reader')[0]
        user.user_permissions.add(Permission.objects.get(codename='view_qrcode'))
        self.client.force_login(user)
        response = self.client.get('{}scanned/qrcode=eyJ2ZXJzaW9uIjoiMSIsInRva2VuIjoiN2JlMTgzYWI4Y2YxYWExYzkxMTcwNDIzNzBiYzQzNDQxNjZjZTgxYWU4NGFlYTkwYWU3ZWFiY2Q5OTJhMGFhMCIsImFjdGlvbiI6ImRvIiwidXJsIjoiaHR0cDovL3Rlc3RzZXJ2ZXIvYXBpL2FkZHBhcnQvMS8xLzUvIn0='.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.headers['Location'], '/scanner/scan/')
        response = self.client.get('{}scanned/qrcode=banana'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.headers['Location'], '/scanner/scan/')
        response = self.client.get('{}scanned/qrcode=eyJ2ZXJzaW9uIjoiMSIsInRva2VuIjoiZTVhNjNmYmViZWE2ZmRmNjFjNmE0NDQ1NDFmN2Q5NzNlNzI3NTA0YjFkMWM2MmMwMjcwNGViZGMzNDJkMTJkZCIsImFjdGlvbiI6ImdvIiwidXJsIjoiaHR0cDovL3Rlc3RzZXJ2ZXIvaW52ZW50b3J5L3BhcnQvVGVzdF9WaWV3X1BhcnRfMC8ifQ=='.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.headers['Location'], 'http://testserver/inventory/part/Test_View_Part_0/')
        response = self.client.get('{}scanned/qrcode=eyJ2ZXJzaW9uIjoiMSIsInRva2VuIjoiMzA3NTc1NjJiMjM3MWI3N2YxMDIyOWNjNmU2ZTkwNTY0MDVmODQ3ZmM3ZTQwYWM2MWU4OWM0ZmIyZmQ4M2U5ZiIsImFjdGlvbiI6ImRvIiwidXJsIjoiaHR0cDovL3Rlc3RzZXJ2ZXIvYXBpL3YxL2ludmVudG9yeS9wYXJ0c3RvcmFnZS8xNzQvYWRkLzEvIn0='.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.headers['Location'], '/scanner/scan/')
        user = User.objects.get_or_create(username='test_code_admin')[0]
        user.user_permissions.add(Permission.objects.get(codename='change_qrcode'))
        self.client.force_login(user)
        try:
            response = self.client.get('{}scanned/qrcode=eyJ2ZXJzaW9uIjoiMSIsInRva2VuIjoiMzA3NTc1NjJiMjM3MWI3N2YxMDIyOWNjNmU2ZTkwNTY0MDVmODQ3ZmM3ZTQwYWM2MWU4OWM0ZmIyZmQ4M2U5ZiIsImFjdGlvbiI6ImRvIiwidXJsIjoiaHR0cDovL3Rlc3RzZXJ2ZXIvYXBpL3YxL2ludmVudG9yeS9wYXJ0c3RvcmFnZS8xNzQvYWRkLzEvIn0='.format(baseurl))
        except ConnectionError as err:
            pass
        except:
            self.fail()
        response = self.client.get('{}scanned/qrcode=eyJ2ZXJzaW9uIjoiMSIsInRva2VuIjoiZTVhNjNmYmViZWE2ZmRmNjFjNmE0NDQ1NDFmN2Q5NzNlNzI3NTA0YjFkMWM2MmMwMjcwNGViZGMzNDJkMTJkZCIsImFjdGlvbiI6ImdvIiwidXJsIjoiaHR0cDovL3Rlc3RzZXJ2ZXIvaW52ZW50b3J5L3BhcnQvVGVzdF9WaWV3X1BhcnRfMC8ifQ=='.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.headers['Location'], '/scanner/scan/')

