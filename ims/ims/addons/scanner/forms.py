from django import forms
from inventory.models import Part


MODEL_CHOICES =(
    ("part","Part"),
    ("assembly","Assembly"),
    ("storage","Storage"),
    ("partstorage","PartStorage"),
    ("kit","Kit"),
)

ACTION_CHOICES =(
    ("add","Add"),
    ("sub","Subtract"),
    ("get","Get"),
)

# Idea to have search functionality to field 't' that does API queries with 'starts with' to the selected 'm'

class ModelGenerateForm(forms.Form):
    m = forms.ChoiceField(choices=MODEL_CHOICES)
    t = forms.ModelChoiceField(queryset=Part.objects.all().exclude(name='deleted'), empty_label="---------", to_field_name="name")
    d = forms.ChoiceField(choices=ACTION_CHOICES)
    n = forms.IntegerField(min_value=0,initial=0)
    c = forms.BooleanField(required=False)

