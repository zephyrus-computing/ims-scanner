from base64 import b64encode,b64decode
from django.conf import settings
from django.shortcuts import get_object_or_404
from hashlib import sha256
from io import BytesIO
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
import logging
import qrcode
import qrcode.image.svg

from inventory.models import Part, Storage
from ims.addons.scanner.classes import QRCodeContent
from ims.addons.scanner.serializers import ScanSerializer

logger = logging.getLogger(__name__)

def find_part(target):
    logger.debug('Request made to _find_part method')
    pieces = target.split('-')
    if len(pieces) == 2:
        logger.debug('Clean split, looking for Part "{}"'.format(pieces[1]))
        return get_object_or_404(Part, name=pieces[1])
    logger.debug('Looking through {} pieces for the Part'.format(len(pieces)))
    for i in range(0, len(pieces)):
        try_name = '-'
        try_name = try_name.join(pieces[-i-1:])
        try:
            part = Part.objects.get(name=try_name)
            if part is not None:
                logger.debug('Found the Part: "{}"'.format(part.name))
                return part
        except:
            logger.info('There was a caught exception while trying to parse the composed Part name "{}"'.format(target))
            continue
    logger.debug('Unabled to find the Part')
    return ''

def find_storage(target):
    logger.debug('Request made to _find_storage method')
    pieces = target.split('-')
    if len(pieces) == 2:
        logger.debug('Clean split, looking for Storage "{}"'.format(pieces[0]))
        return get_object_or_404(Storage, name=pieces[0])
    logger.debug('Looking through {} pieces for the Storage'.format(len(pieces)))
    for i in range(1, len(pieces)):
        try_name = '-'
        try_name = try_name.join(pieces[0:i])
        try:
            storage = Storage.objects.get(name=try_name)
            if storage is not None:
                logger.debug('Found the Storage: "{}"'.format(storage.name))
                return storage
        except:
            logger.info('There was a caught exception while trying to parse the composed Storage name "{}"'.format(target))
            continue
    logger.debug('Unabled to find the Storage')
    return ''

def generate(name, url, action):
    logger.debug('Request made to _generate method')
    filename = settings.MEDIA_ROOT + "/qrcodes/" + name + ".svg"
    try:
        content = QRCodeContent(url, action)
        logger.debug('Created a QRCodeContent object and rendering it to json')
        json = JSONRenderer().render(ScanSerializer(content).data)
        data = b64encode(json)
        logger.debug('Base64Encoded the json and generating the QRCode object ')
        qr = qrcode.QRCode(
            version=1,
            error_correction=error_correction(),
            box_size=settings.ADDON_SCANNER_BOX_SIZE,
            border=settings.ADDON_SCANNER_BORDER,
        )
        qr.add_data(data)
        qr.make(fit=True)
        logger.debug('Added data to the QRCode object to craft the image')
        img = qr.make_image(fill_color=settings.ADDON_SCANNER_FOREGROUND, back_color=settings.ADDON_SCANNER_BACKGROUND, image_factory=qrcode.image.svg.SvgPathImage)
        logger.debug('Attempting to save image as {}'.format(filename))
        img.save(filename)
        logger.info('Created new QRCode ' + filename)
    except Exception as err:
        logger.error(err)
        return ''
    return '{}qrcodes/{}.svg'.format(settings.MEDIA_URL, name)

def error_correction():
    if settings.ADDON_SCANNER_ERROR_CORRECTION.lower() == "l":
        return qrcode.constants.ERROR_CORRECT_L
    if settings.ADDON_SCANNER_ERROR_CORRECTION.lower() == "q":
        return qrcode.constants.ERROR_CORRECT_Q
    if settings.ADDON_SCANNER_ERROR_CORRECTION.lower() == "h":
        return qrcode.constants.ERROR_CORRECT_H
    return qrcode.constants.ERROR_CORRECT_M

def decode(qrcode):
    logger.debug('Request made to _decode method')
    try:
        logger.debug('Parsing the qrcode provided in the request')
        qrcode = qrcode.replace("qrcode=","").replace("%3D","=")
        decoded = b64decode(qrcode)
        stream = BytesIO(decoded)
        logger.debug('Base64Decoded the data and converting to json')
        data = JSONParser().parse(stream)
        serializer = ScanSerializer(data=data)
        if serializer.is_valid():
            logger.debug('Validated the json serializer')
            return serializer.validated_data
    except:
        logger.info('exception occurred while decoding or validated the request data')
    logger.debug('Unable to decode or validate the request data')
    return ''

def compare_token(data):
    logger.debug('Request made to _compare_token method')
    try:
        salt = settings.SECRET_KEY
        newhash = sha256(salt.encode('utf-8') + data['action'].encode('utf-8') + data['url'].encode('utf-8')).hexdigest()
        logger.debug('Generated hash from salt and data')
        return str(newhash) == str(data['token'])
    except:
        logger.debug('Unable to read provided data to generate a hash')
        return False