from django.conf import settings
from hashlib import sha256

class QRCodeContent:
    def __init__(self, url, action):
        self.version = "1"
        self.token = _token(url, action)
        self.action = action
        self.url = url

def _token(url, action):
    salt = settings.SECRET_KEY
    return sha256(salt.encode('utf-8') + action.encode('utf-8') + url.encode('utf-8')).hexdigest()
