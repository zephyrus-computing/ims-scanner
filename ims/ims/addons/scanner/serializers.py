from rest_framework import serializers

class ScanSerializer(serializers.Serializer):
    version = serializers.CharField(max_length=3)
    token = serializers.CharField()
    action = serializers.CharField(max_length=2)
    url = serializers.CharField()
