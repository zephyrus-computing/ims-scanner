from django.contrib.auth.decorators import login_required, permission_required
from django.conf import settings
from django.http import HttpResponseForbidden, HttpResponseBadRequest
from django.shortcuts import render, get_object_or_404, redirect
from json import loads
import logging
import requests

from inventory.models import Part, Storage, PartStorage, Assembly, Kit
from ims.addons.scanner.forms import ModelGenerateForm
from log.logger import getLogger
import ims.addons.scanner.helpers as helpers

logger = logging.getLogger(__name__)
logger2 = getLogger('scanner')

# Make this into a nice ajax call
@permission_required('scanner.add_qrcode')
def create(request):
    logger.debug('Request made to create view')
    generate_form = ModelGenerateForm()
    logger2.info(request.user, 'Accessed Create page')
    return render(request, 'scanner/create.html', {'generate_form': generate_form})

def ajax_create(request):
    logger.debug('Request made to ajax_create view')
    logger2.debug((request.user or 0), 'Request to view ajax_create page')
    if not request.user.has_perm('scanner.add_qrcode'):
        logger.info('HttpResponseForbidden response for ajax_create view')
        logger2.warning((request.user or 0), 'Unauthorized request to path {}'.format(request.path))
        return HttpResponseForbidden()
    action = 'go'
    model = request.GET.get('m')
    display = request.GET.get('c')
    caption = url = ''
    if model == 'assembly':
        logger.debug('Model is Assembly, attempting to locate the target')
        target = request.GET.get('t')
        assembly = Assembly.objects.filter(parent__name=target)[0]
        url = request.build_absolute_uri(assembly.get_absolute_url())
        logger.debug('Found absolute URI')
    if model == 'part':
        logger.debug('Model is Part, attempting to locate the target')
        target = request.GET.get('t')
        part = get_object_or_404(Part, name=target)
        url = request.build_absolute_uri(part.get_absolute_url())
        logger.debug('Found absolute URI')
    if model == 'storage':
        logger.debug('Model is Storage, attempting to locate the target')
        target = request.GET.get('t')
        storage = get_object_or_404(Storage, name=target)
        url = request.build_absolute_uri(storage.get_absolute_url())
        logger.debug('Found absolute URI')
    if model == 'partstorage':
        logger.debug('Model is PartStorage, attempting to locate the target')
        target = request.GET.get('t')
        part = helpers.find_part(target)
        storage = helpers.find_storage(target)
        logger.debug('Getting direction and increment from request data')
        direction = request.GET.get('d')
        if direction == 'get':
            logger.info('Invalid Action selected for partstorage create qrcode request')
            logger2.info(request.user, 'Invalid Action selected for partstorage create qrcode request')
            return HttpResponseBadRequest()
        increment = request.GET.get('n')
        ps = get_object_or_404(PartStorage, part=part, storage=storage)
        url = request.build_absolute_uri('/api/v1/inventory/partstorage/{}/{}/{}/'.format(ps.id, direction, increment))
        logger.debug('Found absolute URI')
        action = 'do'
    if model == 'kit':
        logger.debug('Model is Kit, attempting to locate the target')
        target = request.GET.get('t')
        kit = get_object_or_404(Kit, name=target)
        direction = request.GET.get('d')
        if direction == 'get':
            url = request.build_absolute_uri(kit.get_absolute_url())
        elif direction == 'sub':
            action = 'do'
            increment = request.GET.get('n')
            url = request.build_absolute_uri('/api/v1/inventory/kit/{}/sub/{}/'.format(kit.id, increment))
        logger.debug('Found absolute URI')
    if url == '':
        return HttpResponseBadRequest()
    url = url.replace('http://', 'https://')
    logger.debug('Generating the QR Code')
    qrimage = helpers.generate(target, url, action)
    logger.debug('Generating Caption')
    if model == 'kit':
        if direction == 'get':
            caption = '{} - {}'.format(model, target)
        else:
            caption = '{} - {} - {}:{}'.format(model, target, direction, increment)
    elif model == 'partstorage':
        caption = '{} - {} - {}:{}'.format(model, target, direction, increment)
    else:
        caption = '{} - {}'.format(model, target)
    logger.debug('Rendering request for create view')
    logger2.info(request.user, 'Accessed generated QR code for {}'.format(caption))
    if display == 'true':
        return render(request, 'scanner/code.html', {'qrimage': qrimage, 'caption': caption})
    return render(request, 'scanner/code.html', {'qrimage': qrimage, 'caption': ''})

def home(request):
    logger.debug('Request made to home view')
    logger2.debug((request.user or 0), 'Request to view Home page')
    if request.user.is_authenticated:
        logger.debug('Rendering request for home view')
        logger2.info((request.user or 0), 'Accessed Home page')
        return render(request, 'scanner/home.html')
    else:
        logger.debug('User was not authenticated')
        logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
        return redirect('/accounts/login/?next={}'.format(request.path))

def scan(request):
    logger.debug('Request made to scan view')
    logger2.debug((request.user or 0), 'Request to view Scan page')
    if request.user.is_authenticated:
        logger.debug('Rendering request for scan view')
        logger2.info(request.user, 'Accessed Scan page')
        return render(request, 'scanner/scan.html')
    else:
        logger.debug('User was not authenticated and ALLOW_ANONYMOUS is set to False')
        logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
        return redirect('/accounts/login/?next={}'.format(request.path))

def scanned(request, qrcode):
    logger.debug('Request made to scanned view')
    logger2.debug((request.user or 0), 'Request to parse Scan')
    if request.user.is_authenticated:
        data = helpers.decode(qrcode)
        if data == '':
            logger.debug('Redirecting request to scan view')
            logger2.debug(request.user, 'Empty data, redirecting to Scan page')
            return redirect('/scanner/scan/')
        if helpers.compare_token(data):
            logger.debug('Data token and new hash matched')
            if data['action'] == 'go':
                logger.debug('Data action is "go"')
                if request.user.has_perm('scanner.view_qrcode'):
                    logger.debug('Redirecting request to "{}"'.format(data['url']))
                    logger2.info(request.user, 'Scanned code and redirecting to "{}"'.format(data['url']))
                    return redirect(data['url'])
                logger.debug('Unauthorized request to view scanned url "{}"'.format(data['url']))
                logger2.debug(request.user, 'Unauthorized request to view scanned url "{}"'.format(data['url']))
                return redirect('/scanner/scan/')
            if data['action'] == 'do':
                logger.debug('Data action is "do"')
                if request.user.has_perm('scanner.change_qrcode'):
                    logger.debug('Making request to "' + data['url'] + '"')
                    result = requests.request('GET', data['url'])
                    json = loads(result.text)
                    logger.debug('Loaded the results and rendering the request for scanned view')
                    logger2.info(request.user, 'Scanned code and performed action with result "{}"'.format(json['message']))
                    return render(request, 'scanner/result.html', {'message': json['message'], 'current_count': json['partstorage']['count']})
                logger.debug('Unauthorized request to execute scanned url "{}"'.format(data['url']))
                logger2.debug(request.user, 'Unauthorized request to execute scanned url "{}"'.format(data['url']))
                return redirect('/scanner/scan/')
        logger.debug('Redirecting request to scan view')
        logger2.debug(request.user, 'Token and Hash mismatch, redirecting to Scan page')
        return redirect('/scanner/scan/')
    logger.debug('User was not authenticated and ALLOW_ANONYMOUS is set to False')
    logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
    return redirect('/accounts/login/?next={}'.format(request.path))

def ajax(request):
    logger.debug('Request made to ajax view')
    logger2.debug((request.user or 0), 'Request to ajax view')
    if request.user.has_perm('scanner.add_qrcode'):
        items = []
        selected = request.GET.get('selected')
        if selected == 'assembly':
            logger.debug('Generating a list of Assembly objects')
            # Get all Assemblies
            objects = list(set(Assembly.objects.values_list('parent', flat=True)))
            items = Part.objects.filter(pk__in=objects)
            if not settings.SHOW_DELETED_OBJECTS:
                items = items.exclude(name='deleted')
        if selected == 'part':
            logger.debug('Generating a list of Part objects')
            # Get all Parts
            items = Part.objects.all()
            if not settings.SHOW_DELETED_OBJECTS:
                items = items.exclude(name='deleted')
        if selected == 'storage':
            logger.debug('Generating a list of Storage objects')
            # Get all Storage
            items = Storage.objects.all()
            if not settings.SHOW_DELETED_OBJECTS:
                items = items.exclude(name='deleted')
        if selected == 'partstorage':
            logger.debug('Generating a list of PartStorage objects')
            # Get all PartStorage
            items = PartStorage.objects.all()
            if not settings.SHOW_DELETED_OBJECTS:
                items = items.exclude(part__name='deleted').exclude(storage__name='deleted')
        if selected == 'kit':
            logger.debug('Generating a list of Kit objects')
            # Get all Kit
            items = Kit.objects.all()
            if not settings.SHOW_DELETED_OBJECTS:
                items = items.exclude(name='deleted')
        logger.debug('Rendering request for ajax view')
        logger2.info(request.user, 'Accessed ajax view for {}'.format(selected))
        return render(request, 'scanner/dropdown_list_options.html', {'items': items})
    logger.info('HttpResponseForbidden response for ajax view')
    logger2.warning((request.user or 0), 'Unauthorized request to path {}'.format(request.path))
    return HttpResponseForbidden()

