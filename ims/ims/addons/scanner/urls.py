from django.urls import re_path
from ims.addons.scanner import views

app_name='ims.addons.scanner'
urlpatterns = [
    re_path(r'create/$', views.create, name='create'),
    re_path(r'scan/$', views.scan, name='scan'),
    re_path(r'scanned/(?P<qrcode>[\.\-_\=\w\s]+)$', views.scanned, name='scanned'),
    re_path(r'ajax/$', views.ajax, name='ajax_call'),
    re_path(r'ajax_create/$', views.ajax_create, name='ajax_create'),
    re_path(r'$', views.home, name='home'),
]
