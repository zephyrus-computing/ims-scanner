# IMS-Scanner

The Scanner add-on module for IMS allows the use of QR codes to perform look ups of storage contents, part information, or assembly details and perform quick actions to increment/decrement part counts through the use of PWA pages.

![Generate QR Code](docs/ims-scanner-generate-qr-code.png)

![QR Code for Part - Widget](docs/ims-scanner-generated-qr-code.png)

![Print QR Code](docs/ims-scanner-print-qr-code.png)

![Scanning QR Code](docs/ims-scanner-scan-qr-code.png)

![Mobile Device Scanning QR Code](docs/ims-scanner-scan-qr-code2.png)

![Mobile Device Scanned Part Details Page](docs/ims-scanner-scanned-qr-code.png)


## Installation
Quick installation instructions can be found in the [Quick Install Guide](https://gitlab.com/zephyrus-computing/ims-scanner/-/wikis/quick-install-guide) Wiki section.

Detailed instructions can be found in the [Admin Guide](https://gitlab.com/zephyrus-computing/ims-scanner/-/wikis/admin-guide) Wiki section.

The Debian package can be downloaded from the [Package Registry](https://gitlab.com/zephyrus-computing/ims-scanner/-/packages).

## Roadmap
-	No additional features currently

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

### Development Environment Setup
```
git clone https://gitlab.com/zephyrus-computing/inventory-management-system.git ims
cd ims
./dev.sh setup
cd ..
git clone https://gitlab.com/zephyrus-computing/ims-scanner.git ims-scanner
cd ims-scanner
./dev-scanner.sh setup
source venv-scanner/bin/activate
```
The dev.sh script with the setup option will configure a python virtual environment and install the packages specified in requirements.txt, create a dev db, and execute the superuser creation process.

When you have finished your work, you can run ```deactivate``` to close the environment.

If you need to reset your environment, run the following:
```
./dev-scanner.sh delete
./dev.sh delete
./dev.sh setup
./dev-scanner.sh setup
```

## License
[GPL3](http://choosealicense.com/licenses/gpl-3.0/) 

